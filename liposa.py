#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 abonengo (j.astronomicon@gmail.com)

import os
import sys
import glob
# Import imghdr module (determines the type of image)
import imghdr
# Import required libraries of opencv
import cv2
# Import library for plotting
import matplotlib.pyplot as plt
# Import exif library
import exifread

prog_name = "liposa.py"


def process_tags(filename):
    # Open image file for reading
    with open(filename, 'rb') as f:
        tags = exifread.process_file(f)
    # Return Exif tags
    return tags


def process_image(filename, tags):
    file, extension = os.path.splitext(filename)
    # If image is RAW, get the thumbnail
    if extension in (".CR2", ".cr2", ".ARW", ".arw"):
        thumb = tags['JPEGThumbnail']
        thumb_filename = 'thumb_' + file + '.jpg'
        with open(thumb_filename, 'wb') as f:
            f.write(thumb)
        img = cv2.imread(thumb_filename, 0)
    # Else get the image itself
    else:
        thumb_filename = None
        img = cv2.imread(filename, 0)
    return img, thumb_filename


def display_info(filename):
    # Verify image file
    try:
        image_type = imghdr.what(filename)
    except:
        print("File '%s' does not exist" % filename)
        raise SystemExit('Aborting...')
    if image_type is None:
        print("File '%s' is not an image" % filename)
        raise SystemExit('Aborting...')

    # Load tags
    tags = process_tags(filename)
    try:
        iso = 'ISO ' + str(tags['EXIF ISOSpeedRatings'])
        speed = str(tags['EXIF ExposureTime']) + ' sec'
    except:
        print("File '%s' is not readable or does not contain EXIF tags" % filename)
        raise SystemExit('Aborting...')

    # Read image
    img, thumb_filename = process_image(filename, tags)

    # Define figure
    fig = plt.figure(figsize=(15, 4))

    # Set title
    title = iso + ' - ' + speed

    # Find frequency of pixels in range 0-255
    histogram = cv2.calcHist([img], [0], None, [256], [0, 256])

    # Image subplot
    plt.subplot(1, 3, 1)
    plt.title(filename, y=-0.25)
    plt.imshow(img)

    # Log subplot
    plt.subplot(1, 3, 2)
    plt.plot(histogram, color='b', label='pixels')
    plt.yscale('log')
    plt.axvline(256 * 0.15, 0, 1, c='c', linestyle='dashed', label='15% gap')
    plt.axvline(256 * 0.33, 0, 1, c='g', linestyle='dashed', label='1/3 rule')
    plt.legend()
    plt.grid(True, zorder=5)
    plt.title('Log. scale exposure', y=-0.25)

    # Linear subplot
    plt.subplot(1, 3, 3)
    plt.plot(histogram, color='b', label='pixels')
    plt.axvline(256 * 0.15, 0, 1, c='c', linestyle='dashed', label='15% gap')
    plt.axvline(256 * 0.33, 0, 1, c='g', linestyle='dashed', label='1/3 rule')
    plt.legend()
    plt.grid(True, zorder=5)
    plt.title('Linear scale exposure', y=-0.25)

    fig.suptitle(title, fontsize=20)
    plt.subplots_adjust(top=0.878, bottom=0.219, left=0.04,
                        right=0.99, hspace=0.21, wspace=0.206)
    plt.show()

    # Remove thumbnail if exists
    if not thumb_filename is None:
        os.remove(thumb_filename)


def get_args():
    if len(sys.argv) > 1:
        # Get image file
        filename = sys.argv[1]
        return filename
    else:
        raise SystemExit(print("Info: " + prog_name + " takes at least 1 arguments.\n"
                               "Usage: python3 " + prog_name + " <filename>\n"
                               "Aborting..."))


def main():
    # Get the image's filename
    filename = get_args()
    # Display information
    display_info(filename)


if __name__ == "__main__":
    main()
