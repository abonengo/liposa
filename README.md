# liposa

Liposa (Light pollution saturation) is a Python 3 script helper for astrophotography exposure settings under light polluted skies. It uses exposure settings of an image and to help to choose the best settings depending on sky brightness at your location.

In astrophotography, when imaging under a bright, light-polluted sky, you need a lot more integration time than under a dark sky. But you also need to shorten the exposures because beyond a certain threshold you don't get signal anymore and your image may be overexposed due to the brightness of the sky...

So, what is usally recommanded is to expose the much longer you can until you get your DSLR histogram (on the back of the camera) to 1/3rd from the left.

However, this may not be very practical depending on the screen of your camera or even if you want to compare several photos taken at different exposures.

So the Liposa script helps you to redraw the camera histogram and saves the pictures in order to compare and find the bests setting for your sky polluted location.

![image_2](examples/IMG_1355-plot.png)

## Getting Started

These instructions will get you an copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
Maybe you'll need to install these Python3 libraries:
* matplotlib: ``pip3 install matplotlib``
* opencv: ``pip3 install opencv-python``
* exifread: ``pip3 install exifread``


### Usage
First of all, you need an astrophotography image for which you want to display the histogram information.

Liposa can work on the following image types:
* CR2 (Canon RAW)
* NEF (Nikon RAW)
* ARW (Sony RAW)
* JPEG

Before running the script, be sure to have liposa.py and the image in the same directory.

The program takes only one argument :
* the filename of your image

So, in a terminal run :
```python
python3 liposa.py <filename>
```

For example :
```python
python3 liposa.py image.CR2
```

### Examples
![image_1](examples/IMG_1354-plot.png)
![image_3](examples/IMG_1356-plot.png)

You can see more examples of astrophoto exposure data on my website :

https://www.astronomicon-notebook.com/

Feel free to leave helpful comments!


## Authors

* **Abonengo** - *Initial work* - [Abonengo](https://gitlab.com/abonengo)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* [Samir Kharusi](http://www.samirkharusi.net/sub-exposures.html) for all his work.
